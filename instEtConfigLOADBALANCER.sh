#!/bin/bash

###################################################################################################
###################################################################################################
#######################                                                 ###########################
#######################              Création des ressources            ###########################
#######################                                                 ###########################
###################################################################################################
###################################################################################################


az login

# Création d'un Groupe de ressource

echo "Le nom de votre groupe de ressource"

read groupe

echo "Localisation de votre groupe de ressource"

read location

az group create \
    --name $groupe\
    --location $location

# Création Réseau Virtuel

echo "Le nom de votre réseau virtuel"

read reseauName 

az network vnet create \
    --resource-group $groupe \
    --location $location \
    --name $reseauName \
    --address-prefixes 10.1.0.0/16 \
    --subnet-name myBackendSubnet \
    --subnet-prefixes 10.1.0.0/24



#Création d'une Adresse IP Public

az network public-ip create \
    --resource-group $groupe \
    --name myPublicIP \
    --sku Standard \
    --zone 1 2 3



###################################################################################################
###################################################################################################
#######################                                                 ###########################
#######################             Création serveur MariaDB            ###########################
#######################                                                 ###########################
###################################################################################################
###################################################################################################

echo "Nom du serveur MariaDB"

read nameMariadb

echo "Nom de l'adminMDB"

read admin 

echo "Mot de Passe adminMDB"

read pswdAdmin

az mariadb server create \
    --name $nameMariadb \
    --resource-group $groupe \
    --location $location \
    --admin-user $admin \
    --admin-password $pswdAdmin \
    --sku-name GP_Gen5_2 \
    --version 10.3 \
    --ssl-enforcement Disabled

#Configuration d'une règle de pare-feu

az mariadb server firewall-rule create \
    --resource-group $groupe \
    --server $nameMariadb \
    --name AllowMyIP \
    --start-ip-address 192.168.0.1 \
    --end-ip-address 192.168.0.1

###################################################################################################
###################################################################################################
#######################                                                 ###########################
#######################            Création d'un Load Balancer          ###########################
#######################                                                 ###########################
###################################################################################################
###################################################################################################



echo "Le nom de votre LoadBalancer"

read loadbName

az network lb create \
    --resource-group $groupe \
    --name $loadbName \
    --sku Standard \
    --public-ip-address myPublicIP \
    --frontend-ip-name myFrontEnd \
    --backend-pool-name myBackEndPool

#Création d'un Health Probe

az network lb probe create \
    --resource-group $groupe \
    --lb-name $loadbName \
    --name myHealthProbe \
    --protocol tcp \
    --port 80

# Règle du load Balancer

az network lb rule create \
    --resource-group $groupe \
    --lb-name $loadbName \
    --name myHTTPRule \
    --protocol tcp \
    --frontend-port 80 \
    --backend-port 80 \
    --frontend-ip-name myFrontEnd \
    --backend-pool-name myBackEndPool \
    --probe-name myHealthProbe \
    --disable-outbound-snat true \
    --idle-timeout 15 \
    --enable-tcp-reset true


###################################################################################################
###################################################################################################
#######################                                                 ###########################
#######################     Mise en place  des règles de sécurité       ###########################
#######################                                                 ###########################
###################################################################################################
###################################################################################################


#Création d'un groupe de sécurité

az network nsg create \
    --resource-group $groupe \
    --name myNSG


# Création des règkes d'un réseau de groupe de sécurité 

az network nsg rule create \
    --resource-group $groupe \
    --nsg-name myNSG \
    --name myNSGRuleHTTP \
    --protocol '*' \
    --direction inbound \
    --source-address-prefix '*' \
    --source-port-range '*' \
    --destination-address-prefix '*' \
    --destination-port-range 80 \
    --access allow \
    --priority 200



###################################################################################################
###################################################################################################
#######################                                                 ###########################
#######################          Création d'un serveur Backend          ###########################
#######################                                                 ###########################
###################################################################################################
###################################################################################################


#Création d'une interface réseau pour les machines virtuelles

array=(myNicVM1 myNicVM2)
  for vmnic in "${array[@]}"
  do
    az network nic create \
        --resource-group $groupe \
        --name $vmnic \
        --vnet-name $reseauName \
        --subnet myBackEndSubnet \
        --network-security-group myNSG
  done

#Création des machines Virtuelles

echo "Nom de la vm 1"

read myVM1

echo "Nom de l'utilisateur"

read user

echo "Mot de passe de l'utilisateur 1"

read pswd1

#Machine 1
az vm create \
    --resource-group $groupe \
    --name $myVM1 \
    --nics myNicVM1 \
    --image Debian:debian-11:11-gen2:0.20220503.998 \
    --admin-password $pswd1 \
    --admin-username $user \
    --zone 1 \
    --no-wait

#Machine 2

echo "Nom de la vm 2"

read myVM2

echo "Mot de passe de l'utilisateur 2"

read pswd2

az vm create \
    --resource-group $groupe \
    --name $myVM2 \
    --nics myNicVM2 \
    --image Debian:debian-11:11-gen2:0.20220503.998 \
    --admin-password $pswd2 \
    --admin-username $user \
    --zone 2 \
    --no-wait

# Ajout des machines virtuelles dans le load Balancer

array=(myNicVM1 myNicVM2)
  for vmnic in "${array[@]}"
  do
    az network nic ip-config address-pool add \
     --address-pool myBackendPool \
     --ip-config-name ipconfig1 \
     --nic-name $vmnic \
     --resource-group $groupe \
     --lb-name $loadbName
  done



# Création d'une passerelle NAT

az network public-ip create \
    --resource-group $groupe \
    --name myNATgatewayIP \
    --sku Standard \
    --zone 1 2 3


# Création des ressources de la passerelle NAT

az network nat gateway create \
    --resource-group $groupe \
    --name myNATgateway \
    --public-ip-addresses myNATgatewayIP \
    --idle-timeout 10


# Association de la passerelle NAT avec le sous réseau

az network vnet subnet update \
    --resource-group $groupe \
    --vnet-name $reseauName \
    --name myBackendSubnet \
    --nat-gateway myNATgateway



# Test Load balancer

az network public-ip show \
    --resource-group $groupe \
    --name myPublicIP \
    --query ipAddress \
    --output tsv