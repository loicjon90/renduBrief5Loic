#!/bin/bash
# Mise à jour de Debian pour avoir tous les packages à jour


sudo apt update && sudo apt upgrade -y

# On vérifie le statut de notre compte puis on passe en mode root

sudo whoami

# On installe le package unzipe afin de pouvoir extraire nos archives plus loin

sudo apt install curl unzip -y

###################################################################################################
###################################################################################################
#######################                                                 ###########################
#######################               Installation de LAMP              ###########################
#######################                                                 ###########################
###################################################################################################
###################################################################################################

curl -sSL https://packages.sury.org/apache2/README.txt | sudo bash -x

# On installe apache 2

sudo apt install apache2

# Installation de MariaDB

curl -LsS https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | sudo bash -s -- --mariadb-server-version=10.7 --skip-maxscale --skip-tools

sudo apt update

# On installe le client de MariaDB

sudo apt install mariadb-server mariadb-client

# On sécurise MariaDB 

sudo mysql_secure_installation

# On install PHP

curl -sSL https://packages.sury.org/php/README.txt | sudo bash -x

sudo apt install php8.0 libapache2-mod-php8.0 php8.0-cli php8.0-common php8.0-mbstring php8.0-xmlrpc php8.0-soap php8.0-gd php8.0-xml php8.0-intl php8.0-mysql php8.0-cli php8.0-ldap php8.0-zip php8.0-mcrypt php8.0-curl php8.0-opcache php8.0-readline php8.0-xml php8.0-gd

# On redémarre notre serveur apache2

sudo systemctl restart apache2


###################################################################################################
###################################################################################################
#######################                                                 ###########################
#######################           Installation de WORDPRESS             ###########################
#######################                                                 ###########################
###################################################################################################
###################################################################################################

#On install wordpress

wget https://wordpress.org/latest.zip

# On le place dans le bon dossier et on configure le fichier wp-config.php

sudo mkdir -p /var/www/html/wordpress

sudo unzip latest.zip -d /var/www/html/

sudo chown -R www-data:www-data /var/www/html/wordpress/

sudo find /var/www/html/wordpress -type d -exec chmod 755 {} \;
sudo find /var/www/html/wordpress -type f -exec chmod 644 {} \;

echo "Nom de la base de données"

read DB_NAME

echo "Utilisateur"

read DB_USER

echo "Mot de passe"

read DB_PASSWORD

echo "Nom du serveur"

read DB_HOST

cd /var/www/html/wordpress/

sudo mv wp-config-sample.php wp-config.php

sudo chmod go+w wp-config.php


cd /var/www/html/wordpress/

perl -pi -e "s/database_name_here/$DB_NAME/g" wp-config.php
perl -pi -e "s/username_here/$DB_USER/g" wp-config.php
perl -pi -e "s/password_here/$DB_PASSWORD/g" wp-config.php
perl -pi -e "s/localhost/$DB_HOST/g" wp-config.php


sudo systemctl restart apache2